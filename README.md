# OSOM front-end task

## Overview
Create a flutter application whereas a user I will be able to perform next tasks:
- View the list of the cryptocurrencies in my portfolio including their balances and the corresponding value in euro.
- Click on each currency and visit the dedicated currency page.
- View the currency information including price chart, price alerts settings, currency info, description text.

## Technologies stack

### Front-end
#### Language
- Dart/Flutter (any version you want)

#### Requarements 
- Two screens with/without design
- Chart widget with/without design (libraries avaible)
- Filtering of the list (optional)
- Animations on transitions (optional)
- Unit/Widget test (optional)

#### Design
- Design is provided via the figma [file](https://www.figma.com/file/Hqaa0yVQNe6n7aXBguhPmv/Test-task)

### Backend
- Simple HTTP Node.js server (powered by express for example) in order to get data or local assets or etc.

## Data
- The main list of currencies is provided in currencies-list.json
- Data for an individual asset is provided in assets-data.json

## Images
- You can find images for cryptocurrencies in the images folder

## Helpful links
- [Android devs](https://docs.flutter.dev/get-started/flutter-for/android-devs)
- [IOS devs](https://docs.flutter.dev/get-started/flutter-for/ios-devs)
